{:user
 {:jvm-opts ["-XX:-OmitStackTraceInFastThrow"]
  :plugins [;; Application server
            [lein-immutant "2.0.0"]

            ;; Automated testing
            [lein-cloverage "1.0.2"]
            [lein-test-out "0.3.1"]

            ;; Package management
            [lein-ancient "0.6.8"]
            [lein-licenses "0.2.0"]
            [lein-clojars "0.9.1"]

            ;; Documentation
            [codox "0.8.11"]
            [lein-clojuredocs "1.0.2"]
            [lein-ns-dep-graph "0.1.0-SNAPSHOT"]

            ;; Static analysis
            [lein-typed "0.3.5"]
            [jonase/eastwood "0.2.3"]
            [lein-bikeshed "0.2.0"]
            [lein-kibit "0.1.2"]]}
 :repl {:plugins [[cider/cider-nrepl "0.14.0"]
                  [refactor-nrepl "2.2.0"]]
        :dependencies [[org.clojure/tools.nrepl "0.2.12"]
                       [org.clojars.gjahad/debug-repl "0.3.3"]
                       [difform "1.1.2"]

                       [spyscope "0.1.5"]
                       [org.clojure/tools.trace "0.7.8"]
                       [org.clojure/tools.namespace "0.2.10"]
                       [im.chit/vinyasa.inject "0.3.4"]
                       [im.chit/vinyasa.reflection "0.3.4"]
                       [io.aviso/pretty "0.1.17"]

                       [criterium "0.4.3"]]
        :injections [(require 'spyscope.core)
                     (require '[vinyasa.inject :as inject])
                     (inject/in [vinyasa.inject :refer [inject [in inject-in]]]
                                [clojure.pprint pprint]
                                [clojure.java.shell sh]
                                [clojure.tools.namespace.repl refresh]
                                [clojure.repl doc source]

                                clojure.core
                                [vinyasa.reflection .& .> .? .* .% .%>])]}}
