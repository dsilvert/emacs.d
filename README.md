# emacs.d

## The shoulders of giants

This is an emacs configuration that started some time shortly
after I was born by people way smarter than me. I've realized it
didn't quite suit my needs and so I've started down the
wonderful path of my own config.

The aim of this is to provide an IDE-like experience where, after
learning all the key bindings, a user will have access to more tools
than necessary.

## Goals

* Clojure
* C#
* Windows
* OSX
* Linux

## TODO

* Windows and Linux compatibility
* C# mode
* C# refactoring/compiling

## Overriding configuration

In order to override parts of the configuration, create a directory
named the same as your user name in .emacs.d and add .el files
containing the overrides. All files in this directory will be loaded
after the rest of the configuration has been loaded.

## Requirements

* Emacs 24.4 or greater

## Installation

To install, clone this repo to `~/.emacs.d`, i.e. ensure that the
`init.el` contained in this repo ends up at `~/.emacs.d/init.el`.
Create a symlink from profiles.clj to `~/.lein/profiles.clj`.

The easiest way is to do this is:

````
git clone https://bitbucket.org/dsilvert/emacs.d.git
ln -s ~/.emacs.d/profiles.clj ~/.lein/profiles.clj

On OSX:
brew install aspell --with-lang-en
````

Upon starting up Emacs for the first time, the third-party packages
will be automatically downloaded and installed.


## References
This config was copied from:
* Achint Sandhu - https://github.com/sandhu/emacs.d

This config has been heavily inspired by:
* Magnar Sveen - https://github.com/magnars/.emacs.d
* Steve Purcell - https://github.com/purcell/emacs.d
