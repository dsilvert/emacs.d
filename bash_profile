source /Library/Developer/CommandLineTools/usr/share/git-core/git-prompt.sh

if [ -f $(brew --prefix)/etc/bash_completion ]; then
  . $(brew --prefix)/etc/bash_completion
fi
      
calc () {
    bc -l <<< "$@"
}

alias repl='lein repl :headless'
alias crepl='lein do clean, repl :headless'

alias cow='cowsay -n'
function moo { git "$@" 2>&1 | cow; }

alias g='git'
complete -o default -o nospace -F _git g

alias my-ip='ifconfig | grep "inet "'

export GIT_PS1_SHOWDIRTYSTATE=1
export PS1='\w$(__git_ps1 " (%s)") 🍺  '
