declare -a issues=("Locks Up" "Intermittent Bugs" "Crashes")
declare -a reasons=("Edge Case" "Install Error")

# Seed random generator
RANDOM=$$$(date +%s)

selected_issue=${issues[$RANDOM % ${#issues[@]}]}
selected_reason=${reasons[$RANDOM % ${#reasons[@]}]}

echo "$selected_issue $selected_reason"
