;; Make it hard to quit Emacs - C-x Really Quit
(global-set-key (kbd "C-x r q") 'save-buffers-kill-terminal)
(defun kill-buffer-and-frame ()
  (interactive)
  (kill-buffer)
  (delete-frame))
(global-set-key (kbd "C-x C-c") 'kill-buffer-and-frame)

;; Text size
(global-unset-key (kbd "C-x C-+")) ; don't zoom like this
(bind-key "C-s-+" 'text-scale-increase)
(bind-key "C-s--" 'text-scale-decrease)

(global-set-key (kbd "M-<delete>") 'paredit-forward-kill-word)

;;Case changing functions
(global-unset-key (kbd "M-l")) ;;downcase-word
(global-unset-key (kbd "M-u")) ;;upcase-word
(global-unset-key (kbd "M-c")) ;;capitalize-word
(global-unset-key (kbd "C-x C-l")) ;;downcase-region
(global-unset-key (kbd "C-x C-u")) ;;upcase-region

;;selection
(global-set-key (kbd "s-SPC") 'set-mark-command)
(global-set-key (kbd "s-<backspace>") 'delete-region)

;;changing frames
(global-set-key (kbd "C-`") 'other-frame)

;;redo
(global-set-key (kbd "s-Z") 'undo-tree-redo)

(use-package beginend :ensure t
  :config (beginend-setup-all))

(global-set-key [f8] 'neotree-toggle)